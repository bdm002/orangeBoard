#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 5); // RX, TX

void setup() 
{
  // initialize serial communication
  Serial.begin(9600);
  Serial.println("KocoaFab BLE Test");
  mySerial.begin(9600);    
}

void loop() 
{
  if(mySerial.available())
    Serial.write(mySerial.read());
  if(Serial.available())
    mySerial.write(Serial.read());
}