// https://kocoafab.cc/tutorial/view/522

#include <SoftwareSerial.h>


SoftwareSerial BTSerial(4, 5);  

const int RED_PIN = 11;    
const int GREEN_PIN = 10;  
const int BLUE_PIN = 9;    

int redTemp = 0;
int blueTemp = 0;
int greenTemp = 0;

void setup() {
	pinMode(RED_PIN, OUTPUT);    
	pinMode(GREEN_PIN, OUTPUT);  
	pinMode(BLUE_PIN, OUTPUT);   
	
	Serial.begin(9600);
	BTSerial.begin(9600);
}


void loop() {
	if (BTSerial.available()) {
		if (BTSerial.find("R")) {
			redTemp = BTSerial.parseInt();
		}
		if (BTSerial.find("G")) {
			greenTemp = BTSerial.parseInt();
		}
		if (BTSerial.find("B")) {
			blueTemp = BTSerial.parseInt();
		}
		
		Serial.print("R : ");
		Serial.println(redTemp);
		Serial.print("G : ");
		Serial.println(greenTemp);
		Serial.print("B : ");
		Serial.println(blueTemp); 
		
		analogWrite(RED_PIN, redTemp);
		analogWrite(GREEN_PIN, greenTemp);
		analogWrite(BLUE_PIN, blueTemp);
		
		if (BTSerial.read() == 13) {
			redTemp = 0;
			greenTemp = 0;
			blueTemp = 0;
			Serial.println("Reset");
		}
	}
}