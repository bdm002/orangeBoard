// https://kocoafab.cc/tutorial/view/525

#include <Adafruit_NeoPixel.h>
#include <SoftwareSerial.h>
#define PIN 6

// 오렌지 BLE보드는 4, 5번 핀에 BLE칩이 연결되어 있습니다.
SoftwareSerial BTSerial(4, 5);

// 각 색깔별로 기본값 0을 지정해 줍니다.
int redTemp = 0;
int blueTemp = 0;
int greenTemp = 0;

// 네오픽셀 LED 셋팅(여기선 12개의 Pixel을 사용했는데 연결한 Pixel 수에 맞게 숫자를 바꿔주세요)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(12, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
	strip.begin();
	// 모든 LED Pixel을 Off로 셋팅해줍니다.
	strip.show(); 
	Serial.begin(9600);
	BTSerial.begin(9600);
}

void loop() {

	// 블루투스를 통해 데이터가 들어오면
	if (BTSerial.available()) {
		
		// 받은 데이터를 각 색깔별로 맞게 셋팅해줍니다.
		if (BTSerial.find("R")) {
			redTemp = BTSerial.parseInt();
		}
		if (BTSerial.find("G")) {
			greenTemp = BTSerial.parseInt();
		}
		if (BTSerial.find("B")) {
			blueTemp = BTSerial.parseInt();
		}
		
		// 각 색깔별로 제대로 된 수치가 들어왔는지 시리얼모니터로 확인
		Serial.print("R : ");
		Serial.println(redTemp);
		Serial.print("G : ");
		Serial.println(greenTemp);
		Serial.print("B : ");
		Serial.println(blueTemp);
		
		// 각 색깔별로 셋팅된 값을 이용하여 NeoPixel LED의 색깔을 밝혀줍니다.
		colorWipe(strip.Color(redTemp, greenTemp, blueTemp), 50);
		
		// NeoPixel LED 색깔이 모두 켜졌으면 각 색깔값들을 초기화 해줍니다.
		if (BTSerial.read() == 13) {
			redTemp = 0;
			greenTemp = 0;
			blueTemp = 0;
			Serial.println("Reset");
		}
	}
}


// Neopixel의 색깔을 정해주는 함수
void colorWipe(uint32_t c, uint8_t wait) {
	for (uint16_t i = 0 ; i<strip.numPixels() ; i++) {
		strip.setPixelColor(i, c);
		strip.show();
		delay(wait); 
	}
}