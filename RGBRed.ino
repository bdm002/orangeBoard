// https://kocoafab.cc/tutorial/view/533

#include <SoftwareSerial.h> 

SoftwareSerial BTSerial(4, 5); 
byte buffer[1024]; 
int bufferPosition; 
boolean temp = 0;

void setup() {
  BTSerial.begin(9600); 
  Serial.begin(9600);
  pinMode(5, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  
  bufferPosition = 0; 
}

void loop() {
  if (BTSerial.available()) { 
    byte data = BTSerial.read(); 
    Serial.write(data); 
    buffer[bufferPosition++] = data; 
    
    if (data == 'J') {  
        tone (5, 262); 
        delay(500);
        noTone(5);
    }
    
    if (data == 'D') {  
        tone (5, 294); 
        delay(500);
        noTone(5);
    }
    
    if (data == 'F') {  
        tone (5, 330); 
        delay(500);
        noTone(5);
    }
    
    
    if (data == 'H') {  
        tone (5, 349); 
        delay(500);
        noTone(5);
    }
    
    
    if (data == 'M') {  
      if (temp == 0) {  
        digitalWrite(9, HIGH);
        temp = 1;
      } else {          
        digitalWrite(9, LOW);
        temp = 0;
      }
    }

        if (data == 'N') {  
      if (temp == 0) {  
        digitalWrite(10, HIGH);
        temp = 1;
      } else {          
        digitalWrite(10, LOW);
        temp = 0;
      }
    }

        if (data == 'O') {  
      if (temp == 0) {  
        digitalWrite(11, HIGH);
        temp = 1;
      } else {          
        digitalWrite(11, LOW);
        temp = 0;
      }
    }


        if (data == 'L') {  
      if (temp == 0) {  
        digitalWrite(12, HIGH);
        temp = 1;
      } else {          
        digitalWrite(12, LOW);
        temp = 0;
      }
    }

    if (data == '\n') { 
      buffer[bufferPosition] = '\0';
      bufferPosition = 0; 
    }  
  }
}
